#!/bin/zsh

# System colors

function title() {
  printf "\e[1m%s\e[0m\n" "$1"
}

function show() {
  code=$1;
  text=${2:-$1}
  printf "\e[48;5;${code}m%-3s\e[0m" ${text}
}

title "System colors: "
for i in {0..7}; do show $i; done
for i in {8..15}; do show $i; done
printf "\n\n"

title "Color cube:"
for i in {0..5}; do
  for j in {0..5}; do
    for k in {0..5}; do
      show $((16 + 36 * $j + 6 * $i + $k))
    done
    printf " "
  done
  printf '\n'
done
printf '\n'

title "Grayscale ramp"
for i in {232..255}; do
  show $i
done
printf '\n'

title "Grayscale ramp (%)"
for i in {232..255}; do
  show $i $(( ($i-232) * 100 / 23 ))

done
printf '\n'
